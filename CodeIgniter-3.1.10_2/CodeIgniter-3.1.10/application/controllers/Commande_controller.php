<?php 
   class Commande_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
    public function insererCommande(){
        $this->load->model('Commande');
        $plats = $this->input->post('plats'); 
        $nombres = $this->input->post('nombres'); 
        $this->Commande->insererCommande($plats,$nombres);
        $this->load->view('accueil.php'); 
    }
    
  
   } 
?>