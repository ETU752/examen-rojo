<?php 
class Table extends CI_Model{

    private $idTable;
    private $numero;


    /**
     * Get the value of idTable
     */ 
    public function getIdTable()
    {
        return $this->idTable;
    }

    /**
     * Set the value of idTable
     *
     * @return  self
     */ 
    public function setIdTable($idTable)
    {
        $this->idTable = $idTable;

        return $this;
    }

    /**
     * Get the value of numero
     */ 
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set the value of numero
     *
     * @return  self
     */ 
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    public function __construct($idTable,$numero){
        $this->setIdTable($idTable)->setNumero($numero);
    }
}
?>

