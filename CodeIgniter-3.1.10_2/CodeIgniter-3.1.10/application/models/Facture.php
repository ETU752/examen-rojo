<?php 
class Facture extends CI_Model{

    private $idFacture;
    private $date;
    private $idPersonne;
    private $somme;
    private $idTable;
    private $idCommande;

    public function __construct($idFacture,$date,$idPersonne,$somme,$idTable,$idCommande){
        $this->setIdFacture($idFacture)->setDate($date)->setIdPersonne($idPersonne)-setSomme($somme)->setIdTable($idTable)->setIdCommande($idCommande);
    }

    /**
     * Get the value of idFacture
     */ 
    public function getIdFacture()
    {
        return $this->idFacture;
    }

    /**
     * Set the value of idFacture
     *
     * @return  self
     */ 
    public function setIdFacture($idFacture)
    {
        $this->idFacture = $idFacture;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of idPersonne
     */ 
    public function getIdPersonne()
    {
        return $this->idPersonne;
    }

    /**
     * Set the value of idPersonne
     *
     * @return  self
     */ 
    public function setIdPersonne($idPersonne)
    {
        $this->idPersonne = $idPersonne;

        return $this;
    }

    /**
     * Get the value of somme
     */ 
    public function getSomme()
    {
        return $this->somme;
    }

    /**
     * Set the value of somme
     *
     * @return  self
     */ 
    public function setSomme($somme)
    {
        $this->somme = $somme;

        return $this;
    }

    /**
     * Get the value of idTable
     */ 
    public function getIdTable()
    {
        return $this->idTable;
    }

    /**
     * Set the value of idTable
     *
     * @return  self
     */ 
    public function setIdTable($idTable)
    {
        $this->idTable = $idTable;

        return $this;
    }

    /**
     * Get the value of idCommande
     */ 
    public function getIdCommande()
    {
        return $this->idCommande;
    }

    /**
     * Set the value of idCommande
     *
     * @return  self
     */ 
    public function setIdCommande($idCommande)
    {
        $this->idCommande = $idCommande;

        return $this;
    }
}
?>

