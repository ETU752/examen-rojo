<?php 
   class Personnes_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
  
      public function index()
	{
		$this->load->model('Personnes');
        
        $data = array( 
            'username' => $this->input->post('username'), 
            'mdp' => $this->input->post('mdp') 
            ); 
            
        $resultat=$this->Personnes-> login($data['username'],$data['mdp']); 
        if(isset($resultat['errorLogin'])){
            $data['error'] = " Username erroné ";
            $this->load->view('index',$data); 
        }
        if(isset($resultat['errorPassword'])){
            $data['error'] = " Mot de passe erroné ";
            // mbola tsy mety
            $this->load->view('index',$data); 
        }

        if(isset($resultat['user'])){
            redirect(array('Plat_controller','showPlatDuJour'));
        }

    }
    
  
   } 
?>