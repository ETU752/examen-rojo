<?php 
class Categorie extends CI_Model{

    private $idCategorie;
    private $nom;




    /**
     * Get the value of idCategorie
     */ 
    public function getIdCategorie()
    {
        return $this->idCategorie;
    }

    /**
     * Set the value of idCategorie
     *
     * @return  self
     */ 
    public function setIdCategorie($idCategorie)
    {
        $this->idCategorie = $idCategorie;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function __construct(){}

    public function getCategorie(){
        $resultat=array();
            $count=0;
            $query=$this->db->get('Categorie');
            $resultat = $query->result();
            foreach($query->result() as $ligne)
            {
                $cat= new Categorie();
                $cat->setIdCategorie($ligne->idCategorie);
                $cat->setNom($ligne->nom);
                $resultat[] = $cat;   
            }
            return $resultat;
    } 

}
?>

