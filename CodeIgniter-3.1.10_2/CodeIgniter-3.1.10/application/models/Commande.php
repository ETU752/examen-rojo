<?php 
class Commande extends CI_Model{

    private $idCommande;
    private $listePlat;
    private $nombre;
    private $etat;

   
    /**
     * Get the value of idCommande
     */ 
    public function getIdCommande()
    {
        return $this->idCommande;
    }

    /**
     * Set the value of idCommande
     *
     * @return  self
     */ 
    public function setIdCommande($idCommande)
    {
        $this->idCommande = $idCommande;

        return $this;
    }

    /**
     * Get the value of listePlat
     */ 
    public function getListePlat()
    {
        return $this->listePlat;
    }

    /**
     * Set the value of listePlat
     *
     * @return  self
     */ 
    public function setListePlat($listePlat)
    {
        $this->listePlat = $listePlat;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of etat
     */ 
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set the value of etat
     *
     * @return  self
     */ 
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    public function __construct($idCommande,$listePlat,$nombre,$etat){
        $this->setIdCommande($idCommande)->setListePlat($listePlat)->setNombre($nombre)->setEtat($etat);
    }

    public function inserer($plats, $nombre){
        $data = array(null, $plats,$nombre,0);
        if ($this->db->insert("Commande", $data)) { 
            return true; 
         } 
    } 
    
    public function update($data,$idCommande) { 
        $this->db->set($data); 
        $this->db->where("idCommande", $idCommande); 
        $this->db->update("commande", $data); 
     } 
}
?>

