create database food 

create table Personne(
    idPersonne int primary key not null AUTO_INCREMENT,
    nom varchar (100),
    mdp varchar(100),
    profil varchar(50)
);

create table Categorie(
    idCategorie int primary key not null AUTO_INCREMENT ,
    nom varchar(100)
);


create table Plat(
    idPlat int primary key not null AUTO_INCREMENT ,
    idCategorie int,
    nom varchar(100),
    prix int,
    img varchar(150),
    foreign key (idCategorie) references Categorie(idCategorie)
);

create table LaTable(
    idTable int primary key not null AUTO_INCREMENT,
    numero int 
);


create table Commande(
    idCommande int primary key not null AUTO_INCREMENT,
    listePlat varchar(200),
    nombre int,
    etat int 
);

create table Facture(
    idFacture int primary key not null AUTO_INCREMENT ,
    date Timestamp,
    idPersonne int ,
    somme int ,
    idTable int,
    idCommande int,
    foreign key (idCommande) references Commande(idCommande),
    foreign key (idPersonne) references Personne(idPersonne),
    foreign key (idTable) references LaTable(idTable)
);


create table Menu(
    idMenu int primary key not null AUTO_INCREMENT,
    date Timestamp,
    idPlat int,
    foreign key (idPlat) references Plat(idPlat)
);


create view platdujour as select plat.idPlat, categorie.nom as cat, plat.nom, plat.prix, plat.img, menu.date from plat join menu on menu.idPlat = plat.idPlat join categorie on categorie.idCategorie = plat.idCategorie;

insert into Personne values (null,'Jean','jean','serveur');
insert into Personne values (null,'Mark','mark','serveur');
insert into Personne values (null,'Peter','peter','admin');


insert into Categorie values (null,'Entree');
insert into Categorie values (null,'Resistances');
insert into Categorie values (null,'Desserts');
insert into Categorie values (null,'Boissons');

insert into LaTable values (null,1);
insert into LaTable values (null,2);
insert into LaTable values (null,3);
insert into LaTable values (null,5);


insert into Plat values (null,1,'Rouleau de printemps',12.00,'assets/img/rdp.jpeg');
insert into Plat values (null,1,'Crème de chou-fleur au parfum de curcuma',13.00,'assets/img/chou.jpg');
insert into Plat values (null,1,'Mozzarella Fritta',14.00,'assets/img/mozarella.jpg');
insert into Plat values (null,1,'Crêpe à la vietnamienne',13.00,'assets/img/crepeviet.jpg');
insert into Plat values (null,1,'Croquettes vietnamiennes',12.50,'assets/img/croquette.jpg');
insert into Plat values (null,1,'St Jacques marinés au citron ',14.00,'assets/img/stjacques.jpg');


insert into Plat values (null,2,'Magret de canard au four ',28.00,'assets/img/canard.jpg');
insert into Plat values (null,2,'Riz aux brochettes ',25.00,'assets/img/brochette.jpg');
insert into Plat values (null,2,'Canard rôti au miel et tamarin, légumes croquants',23.50,'');
insert into Plat values (null,2,'Noix de Saint Jacques sautées à la sauce thaï, poêlé de légumes',29.00,'');
insert into Plat values (null,2,'Porc sauté au basilic thaï et riz parfumé',22.50,'');


insert into Plat values (null,3,'Tarte au sucre à l érable ',6.00,'');
insert into Plat values (null,3,'Sorbet aux fruits de la passion, lime et orange',6.00,'');
insert into Plat values (null,3,'Gâteau chocolaté sur croustillant praliné',6.00,'');
insert into Plat values (null,3,'Tarte Vaudoise aux Framboises',6.00,'');
insert into Plat values (null,3,'BLUE RIBBON BROWNIE',6.00,'');
insert into Plat values (null,3,'PROFITEROLE',6.00,'');


insert into Plat values (null,4,'THÉ GLACÉ ',4.00,'');
insert into Plat values (null,4,'LAIT FOUETTÉ ',6.00,'');
insert into Plat values (null,4,'Mojito',9.00,'');
insert into Plat values (null,4,'HEINEKEN 33CL',3.00,'');
insert into Plat values (null,4,'DOUBLE ESPRESSO',4.00,'');

insert into Menu values (null,'11/07/2019',1);
insert into Menu values (null,'11/07/2019',2);
insert into Menu values (null,'11/07/2019',3);
insert into Menu values (null,'11/07/2019',4);
insert into Menu values (null,'11/07/2019',5);
insert into Menu values (null,'11/07/2019',6);