<?php 
class Plat extends CI_Model{

    private $idPlat;
    private $idCategorie;
    private $nom;
    private $prix;
    private $img;

    /**
     * Get the value of idPlat
     */ 
    public function getIdPlat()
    {
        return $this->idPlat;
    }

    /**
     * Set the value of idPlat
     *
     * @return  self
     */ 
    public function setIdPlat($idPlat)
    {
        $this->idPlat = $idPlat;

        return $this;
    }

    /**
     * Get the value of idCategorie
     */ 
    public function getIdCategorie()
    {
        return $this->idCategorie;
    }

    /**
     * Set the value of idCategorie
     *
     * @return  self
     */ 
    public function setIdCategorie($idCategorie)
    {
        $this->idCategorie = $idCategorie;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of img
     */ 
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set the value of img
     *
     * @return  self
     */ 
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    
    public function __construct(){
    }
    
    public function getPlats(){

        $resultat=array();
            $count=0;
            $query=$this->db->get('Plat');
            $resultat = $query->result();
            
        return $resultat;
    }

    public function platsDuJour($jour){
        $resultat=array();
            $count=0;
            $query=$this->db->get_where('platdujour',array('date'=>$jour));
            $resultat = $query->result_array();
            return $resultat;
    }

    public function rechercher($filtre){
        $resultat=array();
        $today = date("y/m/d");
        $filtre="%".$filtre."%";
        $this->db->select('*');
        $this->db->from('platdujour');
        $this->db->where('nom like ',$filtre);
        $this->db->where('date = ',$today);
        $query = $this->db->get();
            // $count=0;
            // $today = date("y/m/d");
            // $query=$this->db->get_where('platdujour',array('nom'=>$filtre,'date'=>$today));
        $resultat = $query->result_array();
        return $resultat;
    }

    public function platParCategorie($categorie){
        $resultat=array();
            $count=0;
            $query=$this->db->get_where('Plat',array('idCategorie'=>$categorie));
            $resultat = $query->result();
            foreach($query->result() as $ligne)
            {
                $plat= new Plat();
                $plat->setIdPlat($ligne->idPlat);
                $plat->setIdCategorie($ligne->cat);
                $plat->setNom($ligne->nom);
                $plat->setPrix($ligne->prix);
                $plat->setImg($ligne->img);
                $resultat[] = $plat;   
            }
            return $resultat;
    } 
    public function platDuJourParCategorie($categorie, $today){
        $resultat=array();
            $count=0;
            $query=$this->db->get_where('Plat',array('idCategorie'=>$categorie));
            $resultat = $query->result();
            foreach($query->result() as $ligne)
            {
                $plat= new Plat();
                $plat->setIdPlat($ligne->idPlat);
                $plat->setIdCategorie($ligne->cat);
                $plat->setNom($ligne->nom);
                $plat->setPrix($ligne->prix);
                $plat->setImg($ligne->img);
                $resultat[] = $plat;   
            }
            return $resultat;
    } 
}
?>

